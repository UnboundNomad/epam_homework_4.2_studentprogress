package model;

public class Student {

 private String name;
 private String surname;
 private StudentProgress studentProgress;

    public Student(String name, String surname, StudentProgress studentProgress) {
        this.name = name;
        this.surname = surname;
        this.studentProgress = studentProgress;
    }

    public StudentProgress getStudentProgress() {
        return studentProgress;
    }

    @Override
    public String toString() {
        return String.format("%-11s %-11s", surname, name);
    }
}
