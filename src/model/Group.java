package model;

public class Group {

    private String title;
    private Student[] students;

    public Group(String title, Student[] students) {
        this.title = title;
        this.students = students;
    }

    public Student[] getStudents() {
        return students;
    }

    @Override
    public String toString() {
        return title;
    }
}
