package model;

import java.util.Arrays;

public class StudentProgress {

    private int[] journal = new int[6];

    public int[] getJournal() {
        return journal;
    }

    public void setJournal(int[] journal) {
        this.journal = journal;
    }

    @Override
    public String toString() {
        return Arrays.toString(journal);
    }
}
