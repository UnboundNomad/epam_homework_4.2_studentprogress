package demo;

import model.Group;
import model.Student;
import services.IMarksCalculation;

public class DemoService {

    private IMarksCalculation service;
    private Group[] groups;

    public DemoService(IMarksCalculation service, Group[] groups) {
        this.service = service;
        this.groups = groups;
    }

    public void execute() {

        for (Group group : groups) {
            showGroup(group);
        }

    }

    private void showGroup(Group group) {

        System.out.printf("%30s %s\n", "Группа:", group.toString());
        System.out.printf("%-2s %-11s %-17s %-15s %s\n",
                "№", "Имя:", "Фамилия:", "Оценки:", "Средний балл:");

        int i = 1;

        for (Student student : group.getStudents()) {
            System.out.printf("%-3d %-11s %-25s %.2f\n",
                    i++, student.toString(), student.getStudentProgress().toString(), service.avgStudentMark(student));
        }

        System.out.printf("\nСредний балл группы: %-5.2f \nКол-во отличников: %-5d \nНа исключение: %d\n",
                service.avgGroupMark(group), service.numberExcellentStudents(group), service.numberLoserStudents(group));

        line();
    }

    private void line() {
        System.out.println("-----------------------------------------------------------");
    }


}
