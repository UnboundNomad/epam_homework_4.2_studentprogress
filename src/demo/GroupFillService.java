package demo;

import model.Group;
import model.Student;
import model.StudentProgress;

import java.util.concurrent.ThreadLocalRandom;

public class GroupFillService implements IGroupFill {

    @Override
    public StudentProgress createStudentProgress() {

        StudentProgress progress = new StudentProgress();
        int length = progress.getJournal().length;
        int min = 20;
        int max = 80;
        int random = getRandom(0, 100);

        if (random >= min && random <= max) {
            progress.setJournal(makeAverage(length));
        }
        if (random < min) {
            progress.setJournal(makeLoser(length));
        }
        if (random > max) {
            progress.setJournal(makeExcellent(length));
        }

        return progress;
    }

    @Override
    public Group createGroup() {

        int size = getRandom(5, 10);
        Student[] students = new Student[size];

        for (int i = 0; i < size; i++) {
            students[i] = createStudent();
        }

        return new Group(createString(5), students);
    }

    @Override
    public Group[] createGroups(int number) {

        Group[] groups = new Group[number];

        for (int i = 0; i < number; i++) {
            groups[i] = createGroup();
        }

        return groups;
    }


    private int[] makeExcellent(int length) {

        int[] result = new int[length];
        int random = getRandom(1, length + 1);

        for (int i = 0; i < random; i++) {
            result[getRandom(1, length)] = 5;
        }

        return result;
    }

    private int[] makeAverage(int length) {

        int[] result = new int[length];

        for (int i = 0; i < length; i++) {
            result[i] = getRandom(3, 6);
        }

        return result;
    }

    private int[] makeLoser(int length) {

        int[] result = new int[length];

        for (int i = 0; i < length; i++) {
            result[i] = getRandom(1, 5);
        }

        return result;
    }

    private Student createStudent() {

        return new Student(createString(getRandom(3, 11)), createString(getRandom(3, 11)), createStudentProgress());

    }

    private String createString(int length) {

        String symbols = "abcdefhijkprstuvwx";
        StringBuilder SB = new StringBuilder(length);
        SB.append(symbols.toUpperCase().charAt(getRandom(0, symbols.length() - 1)));

        for (int i = 1; i < length; i++) {
            SB.append(symbols.charAt(getRandom(0, symbols.length() - 1)));
        }

        return SB.toString();
    }

    private int getRandom (int min, int max) {
        return ThreadLocalRandom.current().nextInt(min, max);
    }

}
