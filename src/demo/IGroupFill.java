package demo;

import model.Group;
import model.StudentProgress;

public interface IGroupFill {

    StudentProgress createStudentProgress ();

    Group createGroup ();

    Group[] createGroups (int number);


}
