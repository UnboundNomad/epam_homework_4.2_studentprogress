import demo.DemoService;
import demo.GroupFillService;
import services.MarkCalculationService;

public class Main {

    public static void main(String[] args) {

        new DemoService( new MarkCalculationService(), new GroupFillService().createGroups(3)).execute();

    }

}