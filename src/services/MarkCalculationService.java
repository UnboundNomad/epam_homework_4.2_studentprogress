package services;

import model.Group;
import model.Student;

public class MarkCalculationService implements IMarksCalculation {


    @Override
    public float avgStudentMark(Student student) {

        int sum = 0;
        int count = 0;

        for (int mark : student.getStudentProgress().getJournal()) {
            if (mark != 0) {
                sum += mark;
                count++;
            }
        }

        return count > 0 ? (float) sum / (float) count : 0;
    }

    @Override
    public float avgGroupMark(Group group) {

        float sum = 0;
        int count = 0;

        for (Student student : group.getStudents()) {
            if (student != null) {
                sum += avgStudentMark(student);
                count++;
            }
        }

        return count > 0 ? sum / (float) count : 0;
    }

    @Override
    public int numberExcellentStudents(Group group) {

        int result = 0;

        for (Student student : group.getStudents()) {
            if (isStudentExcellent (student)) {
                result++;
            }
        }

        return result;
    }

    @Override
    public int numberLoserStudents(Group group) {

        int result = 0;

        for (Student student : group.getStudents()) {
            if (isStudentLoser(student)) {
                result++;
            }
        }

        return result;
    }

    private boolean isStudentExcellent(Student student) {

        return student != null && avgStudentMark(student) == 5;

    }

    private boolean isStudentLoser(Student student) {

        if (student == null) {
            return false;
        }

        for (int mark : student.getStudentProgress().getJournal()) {
            if (mark < 3 && mark > 0) {
                return true;
            }
        }

        return false;
    }
}
