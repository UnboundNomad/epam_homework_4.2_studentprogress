package services;

import model.Group;
import model.Student;

public interface IMarksCalculation {

    float avgStudentMark (Student student);

    float avgGroupMark (Group group);

    int numberExcellentStudents (Group group);

    int numberLoserStudents (Group group);

}
